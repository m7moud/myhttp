package main

import (
	"bytes"
	"crypto/md5"
	"encoding/hex"
	"flag"
	"fmt"
	"net/http"
	"net/url"
	"sync"
)

var (
	wg sync.WaitGroup
)

func main() {
	parallel := flag.Int("parallel", 10, "configure parallel requests, default: 10")
	flag.Parse()

	c := NewClient(*parallel)
	c.MakeRequests(flag.Args())
	c.PrintResponses()
}

type Client struct {
	httpClient http.Client
	parallel   int
	responseCh chan Response
}

func NewClient(parallel int) *Client {
	return &Client{
		httpClient: http.Client{},
		parallel:   parallel,
		responseCh: make(chan Response),
	}
}

type Response struct {
	url      string
	err      error
	bodyHash *string
}

func (r Response) String() string {
	if r.err != nil {
		return fmt.Sprintf("%s %s", r.url, r.err)
	}

	return fmt.Sprintf("%s %s", r.url, *r.bodyHash)
}

// MakeRequests Fires http requests to the given URL queue with a given configurable parallel request count
func (c *Client) MakeRequests(queue []string) chan Response {
	urls := make(chan string, len(queue))

	go func() {
		for _, u := range queue {
			urls <- u
		}
		close(urls)
	}()

	for i := 0; i < c.parallel; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for url := range urls {
				c.makeRequest(url, c.responseCh)
			}
		}()
	}

	return c.responseCh
}

func (c *Client) makeRequest(urlS string, ch chan<- Response) {
	u, err := c.parseURL(urlS)
	if err != nil {
		ch <- Response{
			url: urlS,
			err: fmt.Errorf("error parsing url: %s", err),
		}

		return
	}

	res, err := c.httpClient.Get(u.String())
	if err != nil {
		ch <- Response{
			url: urlS,
			err: fmt.Errorf("error making http request: %s", err),
		}

		return
	}

	buf := new(bytes.Buffer)
	buf.ReadFrom(res.Body)
	hash := md5.Sum(buf.Bytes())

	ch <- Response{
		url:      urlS,
		bodyHash: func() *string { s := hex.EncodeToString(hash[:]); return &s }(),
	}
}

func (c *Client) parseURL(urlS string) (u *url.URL, err error) {
	u, err = url.Parse(urlS)
	if err == nil && u.Scheme == "" {
		u.Scheme = "http"
	}

	return
}

// PrintResponses prints out the responses channel
func (c *Client) PrintResponses() {
	go func() {
		wg.Wait()
		close(c.responseCh)
	}()

	for res := range c.responseCh {
		fmt.Println(res)
	}
}
