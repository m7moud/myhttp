package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

func Test_MakeRequests(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "custom server")
	}))

	defer ts.Close()

	tests := []struct {
		name     string
		parallel int
		queue    []string
		expected []string
	}{
		{
			name:     "with custom server",
			queue:    []string{ts.URL},
			parallel: 1,
			expected: []string{fmt.Sprintf("%s a63aff18ddb057e58c9a26ce95b1d074", ts.URL)}, // MD5 of the response `custom server`
		},
		{
			name:     "with http scheme",
			queue:    []string{"http://www.adjust.com", "http://google.com"},
			parallel: 10,
		},
		{
			name:     "with one domain and no scheme",
			queue:    []string{"adjust.com"},
			parallel: 10,
		},
		{
			name:     "with many domains and configured parallel 3 requests",
			queue:    []string{"adjust.com", "google.com", "facebook.com", "yahoo.com", "yandex.com", "twitter.com", "reddit.com/r/funny", "reddit.com/r/notfunny", "baroquemusiclibrary.com"},
			parallel: 3,
		},
		{
			name:     "empty list",
			queue:    []string{},
			parallel: 100,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := NewClient(tt.parallel)
			cgot := c.MakeRequests(tt.queue)
			got := make([]string, len(cgot))
			go func() {
				wg.Wait()
				close(cgot)
			}()

			for res := range cgot {
				got = append(got, res.String())
			}

			if len(got) != len(tt.queue) {
				t.Errorf("MakeRequests() = %v, want %v", len(got), len(tt.queue))
			}

			if len(tt.expected) > 0 && !reflect.DeepEqual(tt.expected, got) {
				t.Errorf("MakeRequests() = %v, want %v", got, tt.expected)
			}
		})
	}
}
