# myhttp

## Introduction

- I decided to introduce a small improvment to the tool, namely printing errors, if any occured.
- Nothing beyond Go's standard library were used.
- Used Gitlab and added a simple CI setup.
- Added one test case with proper mocked outbound request, using standard library.

## Running the test

`go test -race -v`

## Build

`go build .`

## Further improvements

- Mocking the http client for persise and quick testing like:
  - Testing MD5 hashes.
- Covering parallel requests with tests.
